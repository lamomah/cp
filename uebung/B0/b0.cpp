#include <iostream>
#include <fstream>
#include <cmath>
#include <random>
void HalloWorld()
{
  std::cout << "Hallo World" << std::endl;
}

void Ausloeschung()
{
  std::ofstream file;
  file.open("2a");
  unsigned int points = 25;
  double X = 1;
  for ( unsigned int i = 0; i<points; i++)
  {
    X *= 5;
    double g1 = 1/sqrt(X)-1/sqrt(X+1.);
    double g2 = 1/(sqrt(X*(X+1.))*(sqrt(X+1.)+sqrt(X)));
    file << X << "\t" << g1 << "\t" << g2 << "\t" << g2/g1 << std::endl;
  }
  file.close();

  file.open("2b");
  X = 1;
  for ( unsigned int i = 0; i<points; i++)
  {
    X /=3;
    double g1 = (1-cos(X))/sin(X);
    double g2 = tan(X/2);
    file << X << "\t" << g1 << "\t" << g2 << "\t" << g2/g1 << std::endl;
  }
  file.close();

  points = 100;
  file.open("2c");
  X = 1;
  double delta(1);
  for ( unsigned int i = 0; i<points; i++)
  {
    delta /=1.5;
    double g1 = sin(X+delta)-sin(X);
    double g2 = -2*sin(X)*pow(sin(delta/X),2)+cos(X)*sin(delta);
    file << delta << "\t" << g1 << "\t" << g2 << "\t" << g1/g2 << std::endl;
  }
  file.close();
}

void PI()
{
  //  urng
  std::mt19937 rng;
  rng.seed(123456);
  std::uniform_real_distribution<>dist(0,1);
  
  // output
  std::ofstream file;
  file.open("pi");

  unsigned int points(1e9);
  unsigned int Glumanda = 0;
  for (unsigned int i = 0; i<points;i++)
  {
    double X = dist(rng);
    double Y = dist(rng);
    if(i%100000==0)
      file << X << "\t" << Y << "\n";
    if(X*X+Y*Y < 1) Glumanda++;
  }
  double p = double(Glumanda)/double(points);
  std::cout << "PI="<<4*p << "+/-" << 2*sqrt(p*(1-p)/points) << std::endl;
  file.close();
}

int main()
{
//  HalloWorld();
//  Ausloeschung();
  PI();
  return 0;
}
