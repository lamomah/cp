#!/bin/python
import numpy as np
import matplotlib.pyplot as plt

def plot(filename):
    X,g1,g2,r = np.genfromtxt(filename, unpack=True)
    r[np.isinf(r)]=0
    plt.figure(1)
    plt.subplot(211)
    plt.plot(X, g1)
    plt.plot(X, g2)
    plt.xscale("log")
    plt.yscale("log")
    plt.subplot(212)
    plt.plot(X, r)
    plt.xscale("log")
#    plt.yscale("log")
    plt.show()

plot("2a")
plot("2b")
plot("2c")

X,Y = np.genfromtxt("pi",unpack=True)
VX = X[np.sqrt((X**2+Y**2))<=1]
RX = X[np.sqrt((X**2+Y**2))>1]
VY = Y[np.sqrt((X**2+Y**2))<=1]
RY = Y[np.sqrt((X**2+Y**2))>1]

plt.scatter(VX,VY)
plt.scatter(RX,RY)
plt.show()

