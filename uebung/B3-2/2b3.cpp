#include <iostream>
#include <fstream>
#include <random>
#include <vector>
#include <cstring>
#include <type_traits>


// global random number generator
std::mt19937 rng;


struct microstate
{
  microstate(short rows, short cols, double temp); // constructor
  microstate(){};
  
  std::vector< std::vector<bool> > spins;
  short nRows, nCols, nSpins;
  double T; // temperatur
  double E; // energie
  double M; // magnetisierung

  // funtions
  
  void generateNewState();  // generate a uniform distributed State
  void generateNextState(unsigned int &calls); // generate new state with metropolis
  double calcEnergie();


};

microstate::microstate(short rows, short cols, double temp)
{
  nRows = rows;
  nCols = cols;
  nSpins = nRows*nCols;
  T = temp;
  M = 0;
  spins = std::vector<std::vector<bool>>(nRows,std::vector<bool>(nCols,0));
  calcEnergie();
}

void microstate::generateNewState()
{
  std::uniform_int_distribution<> dist(0,1);
  for(int i=0;i<nRows;i++)
    for(int j=0;j<nCols;j++)
    {
      spins[i][j]=(dist(rng)==1);
      if(spins[i][j])
        M++;
      else M--;

    }
  calcEnergie();
}

void microstate::generateNextState(unsigned int &calls)
{
    std::uniform_int_distribution<> Ydist(0,nRows-1);
    std::uniform_int_distribution<> Xdist(0,nCols-1);
    std::uniform_real_distribution<> Rdist(0,1);
 
    while(true)
    {
      calls++;
      unsigned short Row = Ydist(rng);
      unsigned short Col = Xdist(rng);
      spins[Row][Col]=!spins[Row][Col];

      //calculate Energie 
      if(Col>1){
        if(spins[Row][Col-1])
          E+=0.5;
        else E-=0.5;
      }
      if(Col<nCols-1){
        if(spins[Row][Col+1])
          E+=0.5;
        else E-=0.5;
      }
      if(Row>1){
        if(spins[Row-1][Col])
          E+=0.5;
        else E-=0.5;
      }
      if(Row<nRows-1){
        if(spins[Row+1][Col])
          E+=0.5;
        else E-=0.5;
      }

      if(spins[Row][Col])
        E *=-1;

      if(Rdist(rng) <= exp(2*E/T))
      {
        if(spins[Row][Col])       // Magnetisierung anpassen, Flipp ändert Magnetisierung um 2
          M+=2;
        else M-=2;
        break;
      }

      spins[Row][Col]=!spins[Row][Col];
    }
}

double microstate::calcEnergie()
{
  for(short Row = 0; Row<nRows;Row++)
  {
    for(short Col = 0; Col<nCols;Col++)
    {
      double Ei(0);
      if(Col>1){
        if(spins[Row][Col-1])
          Ei++;
        else Ei--;
      }
      if(Col<nCols-1){
        if(spins[Row][Col+1])
          Ei++;
        else Ei--;
      }
      if(Row>1){
        if(spins[Row-1][Col])
          Ei++;
        else Ei--;
      }
      if(Row<nRows-1){
        if(spins[Row+1][Col])
          Ei++;
        else Ei--;
      }

      if(spins[Row][Col])
        Ei *=-1;
      E+=Ei;
    }
  }
  E/=nRows*nCols;

  return E;


}

/*
//print 2d vector
template<typename T> 
std::ostream &operator<<(std::ostream &out, std::vector<std::vector<T>> spins)
{
  short nRows = spins.size();
  short nCols = spins.size();

  for(int i=0;i<nRows;i++){
    for(int j=0;j<nCols;j++){
      if((j+1)%(nRows) == 0){
         out << spins[i][j] << "\n";
      }else{
        out << spins[i][j] << "\t"; 
      }
    }
  } 
  out << std::endl;
  return out;
}
*/

//print 1d vector
template<typename T>
std::ostream &operator<<(std::ostream &out, std::vector<T> v)
{
  short n = v.size();
  for(int i=0;i<n;i++)
  {
    out << v[i] << "\t";
  } 
  out << std::endl;
  return out;
}



int main(int argc, char **argv)
{
 
  unsigned short nRows = 100;
  unsigned short nCols = 100;
  unsigned long nSteps = 1e5;
  unsigned int calls = 0;
  microstate CurrentState;

  //output
  std::ofstream fileA;
  std::ofstream fileBC;
  
  for(int T = 1;T<4;T+=1)
  {
    std::cout << "Berechne a) b) und c) für T = " << T << std::endl;
    CurrentState = microstate(nRows,nCols,T);     
   // CurrentState.generateNewState();            // generate random init state
    
    //print inital state
    fileA.open("A2-InitT"+std::to_string(T));
    fileA << CurrentState.spins << "\n";        
    fileA.close();
    
  
    fileBC.open("A2-bcT"+std::to_string(T));
    
    calls = 0; 
    for(unsigned long step = 0; step<nSteps; step++)
    { 
      step=calls;
      CurrentState.generateNextState(calls); 
      //cal and store Energie for b) and c)
      fileBC << CurrentState.calcEnergie() << "\t";

      double M = CurrentState.M/CurrentState.nSpins;
      fileBC << M *exp(-CurrentState.E/T)<< "\t";             // Magnetisierung nach (6)
      fileBC << std::abs(M) *exp(-CurrentState.E/T)<< "\n";   // Magnetisierung nach (7)


    }

    fileBC.close();

    //print final state for a)
    fileA.open("A2-FinalT"+std::to_string(T));
    fileA << CurrentState.spins << "\n";
    fileA.close();

  }


  // c teil 2)
  std::cout << "Berechne Magnetisierung in ABhängigkeit der Temperatur" << std::endl;
  fileBC.open("A2-c2");
  for(int i = 0; i< 100;i++) // Teste 100 verschiedene T
  {
    double M = CurrentState.M/CurrentState.nSpins;
    double T = 1+2./100*i;

    fileBC << M *exp(-CurrentState.E/T)<< "\t";             // Magnetisierung nach (6)
    fileBC << std::abs(M) *exp(-CurrentState.E/T)<< "\n";   // Magnetisierung nach (7)
  }
  fileBC.close();

  /*
  // d
  std::cout << "Berechne Spezifische wärme" << std::endl;
  fileA.open("A2-d"); 

  for(int t = 10;t<=30;t+=1)
  {
    double T = double(t)/10;
    CurrentState = microstate(nRows,nCols,T);     
    CurrentState.generateNewState();              
   
    double E = 0, Eq = 0;
    for(unsigned long step = 0; step<nSteps; step++)  
    { 
      CurrentState.generateNextState(calls); 
      E += CurrentState.calcEnergie();
      Eq += CurrentState.E*CurrentState.E;
    }
    E /=nSteps;
    Eq /=nSteps;
    double c = (Eq-E*E)/(T*T*nSteps);
    fileA << T << "\t" << c << "\n";
  }
  fileA.close();*/
  return 0;
}

