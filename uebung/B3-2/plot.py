import numpy as np
import matplotlib.pyplot as plt


# plot a)
aT1Init = np.genfromtxt("A2-InitT1")
aT1Final = np.genfromtxt("A2-FinalT1")
aT3Init = np.genfromtxt("A2-InitT3")
aT3Final = np.genfromtxt("A2-FinalT3")


xedges = np.arange(0,len(aT1Init))
yedges = xedges
x,y = np.meshgrid(xedges, yedges)
plt.pcolormesh(x,y,aT1Init)
plt.savefig("images/aT1init.pdf")
plt.close()
plt.pcolormesh(x,y,aT1Final)
plt.savefig("images/aT1final.pdf")
plt.close()

plt.pcolormesh(x,y,aT3Init)
plt.savefig("images/aT3init.pdf")
plt.close()
plt.pcolormesh(x,y,aT3Final)
plt.savefig("images/aT3final.pdf")
plt.close()


# load b) und c1)

ET1, MT1, AMT1 = np.genfromtxt("A2-bcT1",unpack = True)
ET2, MT2, AMT2=  np.genfromtxt("A2-bcT2",unpack = True)
ET3, MT3, AMT3 = np.genfromtxt("A2-bcT3", unpack = True)


#plot b)
plt.plot(ET1)
plt.xlabel("Zeit")
plt.ylabel("Mittlere Energie")

plt.savefig("images/ET1.pdf")
plt.close()

plt.plot(ET2)
plt.xlabel("Zeit")
plt.ylabel("Mittlere Energie")

plt.savefig("images/ET2.pdf")
plt.close()

plt.plot(ET3)
plt.xlabel("Zeit")
plt.ylabel("Mittlere Energie")

plt.savefig("images/ET3.pdf")
plt.close()


#plot c)

plt.plot(MT1,label="<M>")
plt.plot(AMT1,label="<|M|>")
plt.legend(loc="best")
plt.xlabel("Zeit")
plt.ylabel("Magnetisierung")
plt.savefig("images/MT1.pdf")
plt.close()

plt.plot(MT2,label="<M>")
plt.plot(AMT2,label="<|M|>")
plt.legend(loc="best")
plt.xlabel("Zeit")
plt.ylabel("Magnetisierung")
plt.savefig("images/MT2.pdf")
plt.close()

plt.plot(MT3,label="<M>")
plt.plot(AMT3,label="<|M|>")
plt.legend(loc="best")
plt.xlabel("Zeit")
plt.ylabel("Magnetisierung")
plt.savefig("images/MT3.pdf")
plt.close()

M, AM = np.genfromtxt("A2-c2",unpack = True)

plt.plot(M,label="<M>")
plt.plot(AM,label="<|M|>")
plt.legend(loc="best")
plt.xlabel("Temperatur")
plt.ylabel("Magnetisierung")
plt.savefig("images/MT.pdf")
plt.close()

# plot d)

T, c = np.genfromtxt("A2-d",unpack=True)

plt.plot(T,c)
plt.xlabel("Temperatur")
plt.ylabel("Spezifische Wärme")
plt.savefig("images/cv.pdf")
plt.close()
