#!/bin/python
import numpy as np
import matplotlib.pyplot as plt

# Aufgabe 1

# load samples
samples = np.genfromtxt("A1", unpack=True )
names = ["linSeed1",
         "linSeed2",
         "linSeed3",
         "linSeed4",
         "xorSeed1",
         "xorSeed2"]
# samples[0...5] 0-3 lin gen, 4 and 5 xor shift gen

#plot histograms

for gen,name in zip(samples,names):
    plt.hist(gen,label = name)
    plt.legend(loc="best")
    plt.savefig(name+".pdf")
    plt.close()

# plot korrelation

for gen,name in zip(samples,names):
    plt.plot(gen[0::2],gen[1::2],"bx",label = name)
    plt.legend(loc="best")
    plt.savefig(name+"_cor.png")
    plt.close()

# plot 1e)

from matplotlib.colors import LogNorm

b,c,periode = np.genfromtxt("A1e", unpack=True)

plt.hist2d(b,c,weights=periode,bins=np.arange(1,17)-0.5,norm=LogNorm())
plt.xticks(np.arange(1,16),np.arange(1,16))
plt.yticks(np.arange(1,16),np.arange(1,16))
plt.xlabel("b")
plt.ylabel("c")
plt.colorbar()
plt.savefig("A1e_log.pdf")
#plt.show()
plt.close()

plt.hist2d(b,c,weights=periode,bins=np.arange(1,17)-0.5)
plt.xticks(np.arange(1,16),np.arange(1,16))
plt.yticks(np.arange(1,16),np.arange(1,16))
plt.xlabel("b")
plt.ylabel("c")
plt.colorbar()
plt.savefig("A1e.pdf")
#plt.show()
plt.close()


