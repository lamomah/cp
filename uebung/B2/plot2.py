#!/bin/python
import numpy as np
import matplotlib.pyplot as plt

# Aufgabe 2

# load samples
bins = 30
samples = np.genfromtxt("A2",unpack=True)

# Plot a)
for i, sample in enumerate(samples[:-2]):
    content,_ = np.histogram(sample,bins=bins)
    plt.hist(sample,weights=1/max(content)*np.ones(len(sample)),label="Sample",bins=bins)

    x = np.linspace(-np.pi/2,np.pi/2,100)
    plt.plot(x,np.cos(x),label="cos(x)")
    plt.savefig("A2a"+str(i)+".pdf")

    #plt.show()
    plt.close()

# Plot b)
sigma = 2
mu = 3

def gaus(x,mu,sigma):
    return np.exp(-((x-mu)**2)/(2*sigma**2))/np.sqrt(2*np.pi*sigma**2)

x = np.linspace(-4*sigma+mu,4*sigma+mu,100)

plt.plot(x,gaus(x,mu,sigma),'r-',label="gaus")
plt.hist(samples[-2],normed=True,label="data",bins=30)
plt.savefig("A2b.pdf")
#plt.show()
plt.close()

# Plot c)
sigma = 1 
mu = 3
k = 0.7

x = np.linspace(-4*sigma+mu,4*sigma+mu,100)
xx = np.linspace(0,4*sigma)

plt.plot(x,gaus(x,mu,sigma),'r-',label="gaus")
plt.plot(x,np.exp(-abs(x-mu))*k*sigma,label=r'g(x)=k$\sigma e^{-|x-\mu|}$')
plt.hist(samples[-1],normed=True,label="data",bins=30)
plt.legend(loc="best")
plt.savefig("A2c.pdf")
#plt.show()
plt.close()
