#include <iostream>
#include <fstream>

double rand(unsigned int &r, unsigned int a, unsigned int c,unsigned int m)
{
  r = (a*r+c)%m;
  return (double)r/m;
}

unsigned short XOR(unsigned short &y,
                   unsigned short a,
                   unsigned short b,
                   unsigned short c)
{
  y^=y<<a;
  y^=y>>b;
  y^=y<<c;
  return y;
}



int main(int argc, char **argv)
{
  
  // def r start values for seeds
  unsigned int r1, r2, r4 = r1 = r2 = 1234, r3 = 123456789;
  unsigned short r5,r6 = r5 = 123;

  // open file for output
  std::ofstream file;
  file.open("A1");

  int N(1e5);

  // Generate N values for each seed
  for(int i = 0; i < N; ++i)
  {
    // lin gen
    //            r,    a,  c,         m
    file << rand(r1,   20,120,      6075) << "\t"
         << rand(r2,  137,187,       256) << "\t"
         << rand(r3,65539,  0,     1<<31) << "\t"
         << rand(r4,16807,  0, (1<<31)-1) << "\t"
    // xor gen
    //           r      a,  b,         c
           << XOR(r5,    11,  1,         7) << "\t"
           << XOR(r6,    11,  4,         7)
           << "\n";
  }
  file.close();

  // generate sample for e)

  file.open("A1e");
  unsigned short a = 11;
  for(unsigned short  b = 1; b<16; ++b)
  {
    unsigned short r0 = 123;
    for(unsigned short c = 1; c < 16; ++c)
    {
       unsigned short counter = 0;
      do
      {
        XOR(r0,a,b,c);
        counter++;
      } while(r0 != 123);
      file << b << " " << c << " " << counter << "\n";
    }
  }
  file.close();
  return 0;
}
