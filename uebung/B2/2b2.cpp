#include <iostream>
#include <cmath>
#include <random>
#include <fstream>

// a)
double cos_transform(double r)
{
  return asin(2*r-1);
}

// b)

void box_mueller(std::mt19937 rng, double &z1, double &z2, double mu = 0, double sigma=1)
{
  std::uniform_real_distribution<double> dist(0,1);
  double u1 = dist(rng), u2 = dist(rng);
  z1 = sqrt(-2*log(u1))*cos(2*M_PI*u2)*sigma+mu;
  z2 = sqrt(-2*log(u1))*sin(2*M_PI*u2)*sigma+mu;
}

/*
void box_mueller(std::mt19937 rng, double &z1, double &z2, double mu = 0, double sigma=1)
{
  std::uniform_real_distribution<double> dist(0,1);
  double u1 = dist(rng), u2 = dist(rng);
  z1 = exp(-1./2*(u1*u1+u2*u2));
  z2 = 1./(2*M_PI) * atan(u2/u1);
}
*/
// c)
double gaus(double x, double mu=0, double sigma=1)
{
  return exp(-pow(x-mu,2)/(2*sigma*sigma))/sqrt(2*M_PI*sigma*sigma);
}

double g(double x)
{
  return exp(-std::abs(x));
}

double neumann(std::mt19937 rng, double mu = 0, double sigma = 1,double k = 1)
{
  std::uniform_real_distribution<double> dist(0,1);
  double x,u;
  do
  {
    u = 2*dist(rng)-1;
    std::signbit(u) ? x = -log(std::abs(u)) : x=log(std::abs(u));

    u = dist(rng); 
  } while(u*k*g(x) > gaus(x));
    return x*sigma+mu;
}


// generate
int main(int argc, char **argv)
{

  unsigned int N(1e5);

  // setup rng
  std::mt19937 rng;
  rng.seed(905455455);
  std::uniform_real_distribution<double> dist1(0,1);
  std::uniform_real_distribution<double> dist2(-1,1);

  // open output file
  std::ofstream file;
  file.open("A2");

  // init return variables for box_mueller
  double z1,z2;
  // generate
  for(unsigned int i = 0; i < N; ++i)
  {  
    // a)
    file << cos_transform(dist1(rng))    << "\t"
         << asin(dist1(rng)) << "\t" 
         << asin(dist2(rng)) << "\t";

    // b)
    if(i%2==0)
    {
      box_mueller(rng, z1,z2,3,2);
      file << z1 << "\t";
    }
    else
      file << z2 << "\t";

    // c)
   file << neumann(rng,3,1,0.7) << "\n";


  }
  file.close();
  return 0;
}
