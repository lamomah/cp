import numpy as np
import matplotlib.pyplot as plt

def m(H,beta=1):
    return -np.tanh(beta*H)

N = 1e5
H = np.linspace(-5,5,N)
h,M= np.genfromtxt("A1",unpack=True)
plt.plot(h[h<5],M[h<5],"rx",label="MC")

plt.plot(H,m(H),label="Analytisch")
plt.legend(loc="best")
plt.xlabel("Magnetfeld H in beliebigen Einheiten")
plt.ylabel("Magnetisierung m")
plt.savefig("A1.pdf")
plt.show()
