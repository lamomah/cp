#include <iostream>
#include <fstream>
#include <sstream>
#include <random>
#include <cmath>
#include <omp.h>

double p(double E, double beta = 1) // berechne p 
{
  return exp(-beta*E);
}

double E(bool s, double H) // berechne Energie
{
  if(s)
    return H;
  else
    return -H;
}

#define Hsteps 10000
#define NUM_THREADS 4 
int main(int argc, char **argv)
{
  // init rng
  std::mt19937 rng(1234);
  std::uniform_int_distribution<> dist(0,1);
  std::uniform_real_distribution<> rdist(0,1);

  // Definiere Magnetfeld
  int Hmin(-5), Hmax(5);
  double HstepSize(double(Hmax-Hmin)/Hsteps);
  
  //Definiere start spin
  bool s = true;


  // Schritte pro H
  int Nsteps(1e5);

  // output
  std::ofstream file;
  file.open("A1");

  int counter(0);
  // loop H

  omp_set_num_threads(4);
  #pragma omp parallel for
  for(int i=0; i<Hsteps; i++)
  {
    double H = Hmin +i*HstepSize;
    double Z = 0;
    double M = 0;
    //loop Nsteps per H 
    for(int i = 0; i < Nsteps; i++)
    {
      s = dist(rng)==1;
      Z += p(E(s,H));
      s ? M+=p(E(s,H)) : M-=p(E(s,H));
    }
  
    #pragma omp critical
    {
    file << H << " " << M/Z << "\n";   // m/Z = m
    } 

    std::cout << ++counter << "\r";
  }
  file.close();
}
