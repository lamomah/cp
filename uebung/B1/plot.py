#!/bin/python

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def xtn(x,n):
    return x**n


N, R, σ = np.genfromtxt("1b1",unpack = True)
x,y = np.genfromtxt("1b1-chain", unpack = True)



plt.plot(x,y)
plt.savefig("1b1_exampleWalk.pdf")
plt.show()


args,cov = curve_fit(xtn,N,R)
print(args)
print(np.sqrt(np.diag(cov)))
plt.errorbar(N,R,yerr=σ,fmt="rx")
plt.plot(N,xtn(N,*args))
plt.xlabel("N")
plt.ylabel(r"$R^2$")
plt.savefig("1b1.pdf")
plt.show()

plt.plot(N**args[0],R,"xr")
plt.show()
