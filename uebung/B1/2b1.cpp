#include <iostream>
#include <cmath>
#include <random>

double VolumeSphere()
{
  unsigned int N(1e9);

  // int rng
  std::mt19937 rng;
  rng.seed(123456);
  std::uniform_real_distribution<>dist(0,1);

  double Volume(0);
  for(int i = 0; i<N; i++)
  {
    if(i%int(1e7)==0)
      std::cout << i/1e7 << "%" << "\r" << std::flush;
    double X=dist(rng);
    double Y=dist(rng);
    double Z=dist(rng);
    if(X*X+Y*Y+Z*Z<=1)
      Volume++;
  }
  Volume /= N;
  double sigma = sqrt(Volume*(1-Volume)/N)*8;
  Volume *=8;
  std::cout << "Volume: " << Volume << "+/-" << sigma << std::endl;
  double Vt = 4./3 *M_PI;
  std::cout << "Rel. Abw. The.: " << (Vt-Volume)/(Vt) << std::endl;
  std::cout << Vt << std::endl;
  return 0;
}



double f(double x)
{
  return 1/sqrt(M_PI) * exp(-x*x);
}

double f_sub(double x)
{
  if(x==0)
    return 0;
  return 1/sqrt(M_PI) * exp(-1/(x*x))/(x*x);
}

double simpson(double a, double b, bool substitude=false)
{
  double Int(0);
  double stepwith = (b-a)/10000.;
  for (int i = 0 ; i<10000; i++)
  {
    double A = i*stepwith+a;
    double B = (i+1)*stepwith+a;
    if(substitude)
      Int += (f_sub(A)+4*f_sub((A+B)/2)+f_sub(B));
    else Int += (f(A)+4*f((A+B)/2)+f(B));
  }
  return stepwith/6. * Int;
}

void MCI(double T, double &Int, double &Var, bool substitude=false)
{
  unsigned int N(1e9);

  // init rng
  std::mt19937 rng;
  rng.seed(std::random_device()());
  std::uniform_real_distribution<>dist(0,1);

  Int=0;  
  Var=0;

  // mc integration
  // Integriere alle Teilintevalle Paralell, um etwas weniger zaheln zihen zu müssen
  for(int i = 0; i<N; i++)
  {
    //fortschrittsanzeige
    if(i%int(1e7)==0)
      std::cout << i/1e7 << "%" << "\r" <<std::flush;

    double X = dist(rng); // draw x

    double tmp;
    if(substitude) // substitude x to 1/u 
      tmp = T*f(1/(T*X))/(X*X);
    else tmp= T*f(X*T);

    Int += tmp/N;
    Var += tmp*tmp/N;   
  }

  // berechne varianz
  Var -= Int*Int;
  Var /= N;
}



int main()
{
  VolumeSphere();

  double Int[3][2];
  double Var[3];
  double T[] = {1,1.1631,1};
  bool sub[] {0,0,1};

  for(int i=0;i<3;i++) // loop T
  {
    std::cout << "Intervall: [0," << T[i]<<"], sub = " << sub[i]<< "\n";
    
    MCI(T[i],Int[i][0],Var[i],sub[i]);
    Int[i][1] = simpson(0,T[i],sub[i]);
        
    std::cout << "MC est.: " << Int[i][0] << "+/-" << sqrt(Var[i]) << "\n"
    << "Simpson est.: " << Int[i][1] << "\n" << std::endl;
  }

  std::cout << "Intervall: [-inf,-1]\n"
            << "MC set.: " << Int[2][0] << "+/-" << sqrt(Var[2]) << "\n"
            << "Simpson est.: " << Int[2][1] << "\n" << std::endl;

  Int[2][0] += Int[0][0];
  Int[2][1] += Int[0][1];
  Var[2]    += Var[0];

  std::cout << "\nIntervall: [-inf,inf]\n"
            << "MC set.: " << 2*Int[2][0] << "+/-" << 2*sqrt(Var[2]) << "\n"
            << "Simpson est.: " << 2*Int[2][1] << std::endl;

  Int[2][0] += Int[1][0];
  Int[2][1] += Int[1][1];
  Var[2]    += Var[1];

  std::cout << "\nIntervall: [-inf,1.1631]\n"
            << "MC set.: " << Int[2][0] << "+/-" << sqrt(Var[2]) << "\n"
            << "Simpson est.: " << Int[2][1] << std::endl;



  return 0;
} 
