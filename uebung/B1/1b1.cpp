#include <iostream>
#include <fstream>
#include <cmath>
#include <cstring>
#include <random>

void writeChain(short int(*array)[2],unsigned short int N)
{
  std::ofstream file;
  file.open("1b1-chain");

  for(unsigned short int i = 0; i < N; i++)
  {
    file << array[i][0] << "\t" << array[i][1] << "\n";  
  }
  file.close();
}

double SAW(unsigned short int N, std::mt19937 &rng, bool writeToFile = false)
{
  std::uniform_int_distribution<unsigned short int>dist(0,3); 

  short int saw[N+1][2];
  short int grid[2*N+1][2*N+1];
  std::memset(grid, -1, sizeof(grid)); // init grid with -1 
  grid[N][N] = 0;


  // start locations
  saw[0][0] = N;
  saw[0][1] = N;

  double rsquare(0); // mean square cluster size 

  for(short int l=0; l<N;++l)
  {
    short int x = saw[l][0];
    short int y = saw[l][1];
    
    unsigned short int directions(0);
    // check for Dead End
    grid[x+1][y] != -1 ? : directions++;
    grid[x-1][y] != -1 ? : directions++;
    grid[x][y+1] != -1 ? : directions++;
    grid[x][y-1] != -1 ? : directions++;
    if(directions==0)
      return -1;

    while(grid[x][y] != -1) // find new valid location
    {
      x = saw[l][0];
      y = saw[l][1];

      unsigned short int rand = dist(rng); 
      switch(rand)
      {
        case 0: x++; 
                break;
        case 1: y++; 
                break;
        case 2: x--; 
                break;
        case 3: y--; 
                break;
        default: std::cerr << "Something went wrong in Switch" << std::endl;

      } // end switch(rand)
    } // end while(grid[x][y] !=1)
    
    // set new location
    saw[l+1][0]=x;
    saw[l+1][1]=y;
    grid[x][y]=l+1;

    rsquare += pow(x-N,2)+pow(y-N,2);
         

  } // end for(l<N)
  if (writeToFile)
    writeChain(saw,N);
  return rsquare/sqrt(N);
}

int main(int argc, char **argv)
{

  std::mt19937 rng;
  if(argc==2) // chose random seed if none given
    rng.seed(atoi(argv[1]));
  else
    rng.seed(std::random_device()());

  short int Nmin   = 10;
  short int Nmax   = 60;
  short int Nsteps = 5;
  unsigned int nClusters(1e5);

  // output
  std::ofstream file;
  file.open("1b1");


  for(short int N = Nmin; N<=Nmax;N+=Nsteps)
  {
    double rsquareMean(0);
    double rsquareVar(0);
    for(unsigned int iCluster = 0; iCluster < nClusters; ++iCluster)
    {
      double tmp = SAW(N,rng);
      if(tmp == -1)
      {
        iCluster--;
        continue;
      }
      rsquareMean += tmp;
      rsquareVar  += tmp*tmp;
    }
    rsquareMean /= nClusters;
    rsquareVar  /= nClusters;
    rsquareVar  -= rsquareMean*rsquareMean;
    rsquareVar /= nClusters;

    file << N << "\t" << sqrt(rsquareMean) << "\t" << sqrt(sqrt(rsquareVar)) << std::endl;
    std::cout << N << "\t" << sqrt(rsquareMean) << "\t" << sqrt(sqrt(rsquareVar)) << std::endl;

  }
  file.close();
  //while(SAW(150,rng,true)==-1);

  return 0;
}
