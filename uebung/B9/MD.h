// MD.h
#ifndef MD_H
#define MD_H

#include <ostream>
#include <string>
#include <eigen3/Eigen/Dense>


class MD
{
  public:
    MD(double Lx, double Ly, double Lz, double cutoff, unsigned int numberOfParticles, double stepwith);
    ~MD();

    void runE(unsigned int nSteps, double T,std::string  outputFileName);
    void runB(unsigned int nSteps, double T,std::string  outputFileName = "B");
    void runC(unsigned int nSteps, unsigned int burnin,double T, double dr,std::string  outputFileName = "C");

    void reset();
    void reset(double stepwith);


    void stepVerlet();
    void stepVerletV(); // Alternative implemantation funktioniert eher schlechter


    
    void printCurrentState(std::string fileName);
    // observabeln
    Eigen::Vector3d getSchwerpunktsGeschwindigkeit();
    double getEkin();
    double getEpot();
    double getT();
    void updateG(double *g, double dr);

  private:
    struct particle
    {
      particle();
      particle(double px, double py, double pz);

      Eigen::Vector3d r,rvor,v,F,Fnext;
      double Epot;

      double getDistance(particle P); 
      particle& operator=(particle P);

      friend std::ostream& operator<< (std::ostream& stream, particle& P)
      {
        stream << P.r.transpose() << " \t " << P.v.transpose() << " \t " << P.F.transpose();
        return stream;
      }

    } *particles;

    unsigned int N;
    double h, rc;
    Eigen::Vector3d L;


    void updateForces();
    void updateForces(particle &A, particle &B);

    void setStartLocations();
    void setStartVelocity();

    double gradV(double r);
    double V(double r);
    
    void periodicBoundaryConditions(Eigen::Vector3d &V);

    void setTemperature(double T);




    
};

#endif
