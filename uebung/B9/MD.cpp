// MD.cpp

#include "MD.h"
#include <cmath>
#include <iostream>
#include <fstream>


MD::MD(double pLx, double pLy, double pLz, double cutoff, unsigned int numberOfParticles, double stepwith)
{
  L = Eigen::Vector3d(pLx,pLy,pLz);
  N = numberOfParticles;
  h = stepwith;
  rc = cutoff;

  particles = new particle[N];

  
  setStartLocations(); 
  setStartVelocity();

  updateForces(); 

}// Ende Konstruktor

MD::~MD()
{
  delete[] particles;
}

void MD::runE(unsigned int nSteps, double T, std::string outputFileName)
{
  std::cout << "Run e):" << std::endl;
  setTemperature(T);

  printCurrentState(outputFileName+"/-1");
  for(unsigned int i = 0; i < nSteps; i++)
  {
    if(i%(nSteps/1000)==0)
      std::cout << i*100./nSteps << " %\r" <<std::flush;

    stepVerlet();
    printCurrentState(outputFileName+"/"+std::to_string(i));
  }


}

void MD::runB(unsigned int nSteps,double T, std::string outputFileName)
{
  std::cout << "Run b):" << std::endl;
  setTemperature(T);

  std::ofstream file;
  file.open(outputFileName);
  for(unsigned int i = 0; i< nSteps ; i++)
  {
    if(i%(nSteps/1000)==0)
      std::cout << i*100./nSteps << " %\r" <<std::flush;
    file << getSchwerpunktsGeschwindigkeit().transpose() << "\t" << getT() << "\t" << getEkin() << "\t" << getEpot() << "\n";
    stepVerlet();
  }
  std::cout << "\033[F" << "\r" << "Run b): \u2714" << std::endl;
}

void MD::runC(unsigned int nSteps,unsigned int burnin, double T, double dr, std::string outputFileName)
{
  std::cout << "Run c) mit T = " << T<<":" << std::endl;
  setTemperature(T);

  double Tmean(0);
  unsigned int n = L(0)/2/dr;
  double *g = new double[n];
  std::memset(g, 0, n*sizeof(double));
 
  std::ofstream file;
  file.open(outputFileName+std::to_string(T));

  std::cout << "Äquilibrieren:" << std::endl;
  for(unsigned int i = 0; i<burnin ; i++)
  {
    if(i%(burnin/1000)==0)
      std::cout << i*100./burnin<< " %\r" <<std::flush;
    stepVerlet();
  }
  std::cout << "\033[F" << "\r" << "Äquilibrieren: \u2714" << std::endl;


  std::cout << "Bestimme T und g:" << std::endl;
  
  for( unsigned int i = 0; i < nSteps; i++)
  {
    printCurrentState(outputFileName+"/"+std::to_string(i/1000));


    if(i%(nSteps/1000)==0)
    {
      std::cout << i*100./nSteps << " %\r" <<std::flush;
      //std::cout << i << " %\r" <<std::flush;
    }

    Tmean += getT()/nSteps;
    updateG(g,dr);
    stepVerlet();
  }
  std::cout << "\033[F" << "\r" << "Bestimme T und g: \u2714" << std::endl;

  file << Tmean << "\n\n";

  for(unsigned int i = 0; i< n; i++)
  {
    file << g[i]/nSteps << "\n";
  }
  delete[] g;
  file.close();

}
void MD::reset()
{
  delete[] particles;
  particles = new particle[N];
  setStartLocations();
  setStartVelocity();
  updateForces();
}
void MD::reset(double stepwith)
{
  h = stepwith;
  reset();
}


void MD::stepVerlet()
{
  #pragma omp parallel for
  for(unsigned int i = 0; i<N; i++)
  {
    particles[i].r += particles[i].v*h + particles[i].Fnext*pow(h,2);
    periodicBoundaryConditions(particles[i].r);
  }
  updateForces();
  #pragma omp parallel for
  for(unsigned int i = 0; i<N; i++)
  {
    particles[i].v += h*(particles[i].F+particles[i].Fnext)/2;
  }
}
void MD::stepVerletV()
{
  #pragma omp parallel for
  for(unsigned int i = 0; i<N; i++)
  {
     Eigen::Vector3d temp = particles[i].r;
    if(particles[i].rvor == Eigen::Vector3d(-1,-1,-1))
    {
      particles[i].r += particles[i].v*h +particles[i].Fnext*pow(h,2)/2;
      particles[i].rvor = temp;
    }
    else
    {    
      particles[i].r = 2 * particles[i].r -particles[i].rvor+h*h*particles[i].Fnext;
      particles[i].rvor = temp;
    } 
    particles[i].v = (particles[i].r-particles[i].rvor)/2/h;

    periodicBoundaryConditions(particles[i].r);

   }
  updateForces();
}

void MD::printCurrentState(std::string fileName)
{
  std::ofstream file;
  file.open(fileName);
  for(unsigned int i = 0; i<N; i++)
  {
    file << particles[i] << "\n";
  }
  file.close();
}
/*
void MD::updateForces(particle &A, particle &B)
{
  Eigen::Vector3d n(0,0,0),F(0,0,0),r,nr;
  double x,y,z=y=x=0, Epot=0;
  r=B.r-A.r;

  bool breakx = false, breaky, breakz;

  while(!breakx)
  {
    y=0;
    breaky=false;
    while(!breaky)
    {
      z=0;
      breakz=false;
      while(!breakz)
      {
        n = Eigen::Vector3d(x,y,z);
        nr=r+n.cwiseProduct(L);
        if(nr.norm()<rc)
        {
          F-=nr/nr.norm()*gradV(nr.norm());
          Epot+=V(nr.norm());
        }
        else breakz=true;

        if(n!=Eigen::Vector3d(0,0,0))
        {
          nr=r-n.cwiseProduct(L);
          if(nr.norm()<rc)
          {
            F-=nr/nr.norm()*gradV(nr.norm());
            Epot += V(nr.norm());
            breakz=false;
          }
        } 
        if(breakz)
        {
          if(z==0)
          {
            breaky=true;
            if(y==0)
            {
              breakx=true;
            }
          }
        }
        if(L(2)==0) // 2D case
          breakz = true;
        z+=1;
      }
      y+=1;
    }
    x+=1;
  }
  #pragma omp critical
  {
    A.Fnext-=F;
    B.Fnext+=F;
    A.Epot+=Epot;
    B.Epot+=Epot;
  }
}
*/

void MD::updateForces(particle &A, particle &B)
{
  Eigen::Vector3d n(0,0,0),F(0,0,0),r,nr;
  double x,y,z=y=x=0, Epot=0;
  r=B.r-A.r;

  for(x = -10; x<10; x++)
    for(y = -10; y<10;y++)
      for(z = -10; z<10; z++)
      {
        n = Eigen::Vector3d(x,y,z);
        nr=r+n.cwiseProduct(L);
        if(nr.norm()<rc)
        {
          F-=nr/nr.norm()*gradV(nr.norm());
          Epot+=V(nr.norm());
        }
        if(L(2)==0) break;
      }
  #pragma omp critical
  {
    A.Fnext-=F;
    B.Fnext+=F;
    A.Epot+=Epot;
    B.Epot+=Epot;
  }

}
void MD::updateForces()
{
  #pragma omp parallel for
  for(unsigned int i = 0; i< N; i++)
  {

    particles[i].F=particles[i].Fnext;
    particles[i].Fnext = Eigen::Vector3d(0,0,0);
    particles[i].Epot = 0;
  }
  #pragma omp parallel for
  for(unsigned int i = 0; i < N; i++)
    for(unsigned int j = i+1; j <N; j++)
    {
      updateForces(particles[i],particles[j]);
    }
}

double MD::gradV(double r)
{
  //return 24*(pow(r,6)-2)/pow(r,13);
  return 48 *pow(r,-7)*(0.5-pow(r,-6));
}

double MD::V(double r)
{
  return 4*(pow(r,-12)-pow(r,-6))-4*(pow(rc,-12)-pow(rc,-6));
}

void MD::setStartLocations()
{
  double V,u;
  Eigen::Vector3d r;
  if (L(2) == 0)  // 2D case
  {
    V = L(0)*L(1);
    u = std::sqrt(V/N);
    r=Eigen::Vector3d(std::fmod(u/2,L(0)),std::fmod(u/2,L(1)),0.);
  }
  else
  {
    V = L(0)*L(1)*L(2);
    u = pow(V/N,1/3.); 
    r = Eigen::Vector3d(std::fmod(u/2,L(0)),std::fmod(u/2,L(1)),std::fmod(u/2,L(2)));
  }

  for(unsigned int i = 0; i<N;i++)
  { 
    particles[i].r=r;
    
    r(0)+=u;
    if (r(0)>L(0))
    {
      r(0)=u/2;
      r(1)+=u;
      if(r(1)>L(1))
      {
        r(1)=u/2;
        r(2)+=u;
      }
    }
  }
}

void MD::setStartVelocity()
{
  if(L(2)==0)  // 2d case
  {
    #pragma omp parallel for
    for(unsigned int i = 0; i<N; i++)
      particles[i].v(2)=0;
  }

  Eigen::Vector3d v = getSchwerpunktsGeschwindigkeit();
  #pragma omp parallel for
  for(unsigned int i = 0; i<N;i++)
  {
    particles[i].v-=v;

  }
}

void MD::periodicBoundaryConditions(Eigen::Vector3d &V)
{
  int dim = L(2)==0 ? 2 :3;
  for(int i =0; i< dim;i++)
    V(i)= V(i) < 0 ? std::fmod(V(i),L(i))+L(i) : std::fmod(V(i),L(i));
}

void MD::setTemperature(double T)
{
  double E = getEkin();
  double currentT = E*2/3/N;      //kb = 1
  double ratio = T/currentT;

  for(unsigned int i = 0; i<N; i++)
  {
    particles[i].v*=std::sqrt(ratio);
  }
}
// observabeln

Eigen::Vector3d MD::getSchwerpunktsGeschwindigkeit()
{
  Eigen::Vector3d v(0,0,0); 
  for(unsigned int i = 0; i < N; i++)
  { 
    v+=particles[i].v; 
  }
  v/=N;
  return v;
}

double MD::getEkin()
{
  double E=0;
  for(unsigned int i = 0; i<N;i++)
  {
    E+=particles[i].v.dot(particles[i].v);
  }
  return E/2;            // m=1
}
double MD::getEpot()
{
  double E = 0;
  for(unsigned int i= 0; i<N; i++)
  {
    E+=particles[i].Epot;
  }
  return E/2; // doppeltzählung
}

double MD::getT()
{
  return getEkin()*2/3/N;
}


// Beim anblick dieser Funktion möchte ich selbstmord umgehen
// evtl. geht das auch eifacher aber es ist mitten in der nacht und ich habe grade
// keine lust mir dazu weiter gedanken zu machen wie man das effizienter lösen könnte
void MD::updateG(double *g, double dr)
{
  unsigned int i,j;
  int x,y,z;
  unsigned int nSteps = L(0)/2./dr;
  for(i = 0; i<N; i++)
  {
    for(j=0; j<N; j++)
    {
      if(i==j) continue;
      for(x=-1;x<=1;x++)
      {
        for(y=-1;y<=1;y++)
        {
          for(z=-1;z<=1;z++)
         { 
            double r = (particles[i].r-particles[j].r-L.cwiseProduct(Eigen::Vector3d(x,y,z))).norm();
            //if(r<10)
            //  std::cerr << r << "\n";
            unsigned int l = r/dr;
            if(l+1<nSteps)
            {
              double Norm;
              L(2)==0 ? Norm=N*M_PI*(pow(dr*(l+1),2)-pow(dr*l,2)) : Norm = N*4/3* M_PI*(pow(dr*(l+1),2)-pow(dr*l,2));
              g[l]+= 1/Norm;
            }
            if(L(2)==0) break;
          }//End z mirror
        }// End y mirror
      }// End x mirror
    }//end loop Particle j

  }// End loop Partricle i
}//End updateG

/*
// Beim anblick dieser Funktion möchte ich selbstmord umgehen
// evtl. geht das auch eifacher aber es ist mitten in der nacht und ich habe grade
// keine lust mir dazu weiter gedanken zu machen wie man das effizienter lösen könnte
void MD::updateG(double *g, unsigned int nSteps)
{
  unsigned int i,j,step;
  int x,y,z;
  double dr = 2*L.maxCoeff()/nSteps;
  for(i = 0; i<N; i++)
  {
    #pragma omp parallel for
    for(step = 0; step < nSteps; step++)
    {
      for(j=0; j<N; j++)
      {
        if(i==j) continue;
        for(x=-3;x<=3;x++)
        {
          for(y=-3;y<=3;y++)
          {
            for(z=-3;z<=3;z++)
           {

              double r = (particles[i].r-particles[j].r-L.cwiseProduct(Eigen::Vector3d(x,y,z))).norm();
              if(dr*(step-1)< r && r <= dr*step)
              {
                double Norm;
                L(2)==0 ? Norm=N*M_PI*(pow(dr*step,2)-pow(dr*step,2)) : Norm = N*4/3* M_PI*(pow(dr*step,2)-pow(dr*step,2));
                g[step]+= 1/Norm;
              }
              if(L(2)==0) break;
            }//End z mirror
          }// End y mirror
        }// End x mirror
      }//end loop Particle j
    }//End loop g_step
  }// End loop Partricle i
}//End updateG
*/




// functions of particle struct

MD::particle::particle()
{
  r = Eigen::Vector3d(0,0,0);
  rvor = Eigen::Vector3d(-1,-1,-1);
  v = Eigen::Vector3d::Random();
  F = Eigen::Vector3d(0,0,0);
  Fnext = Eigen::Vector3d(0,0,0);
}
MD::particle::particle(double px, double py, double pz)
{
  r = Eigen::Vector3d(px,py,pz);
  rvor = Eigen::Vector3d(-1,-1,-1);
  v = Eigen::Vector3d::Random();
  F = Eigen::Vector3d(0,0,0);
  Fnext = Eigen::Vector3d(0,0,0);
}
MD::particle& MD::particle::operator=(particle P)
{
  r=P.r;
  v=P.v;
  F=P.F; 
  return *this;
}


double MD::particle::getDistance(particle P)
{
  Eigen::Vector3d temp = (r-P.r);
  return std::sqrt(temp.dot(temp));
}


