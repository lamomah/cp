#include <iostream>
#include <fstream>
#include <sstream>
#include <random>
#include <cmath>
#include <omp.h>

#include "MD.h"

//#define Hsteps 10000
//#define NUM_THREADS 4 
//#pragma omp parallel for
// omp_set_num_threads(4);

int main(int argc, char **argv)
{
  MD Simulation(8,8,0,4,16,0.0001);
  
    
 
//  Simulation.reset();
//  Simulation.runB(10000,0.01,"B0.01");
  Simulation.reset();
  Simulation.runB(50000,1,"B1");
//  Simulation.reset();
//  Simulation.runB(10000,100,"B100");

  
  Simulation.reset();
  Simulation.runC(100000,1000,0.01,0.01);
  Simulation.reset();
  Simulation.runC(100000,1000,1,0.01);
  Simulation.reset();
  Simulation.runC(100000,1000,100,0.01);

  /*
  Simulation.reset();
  Simulation.runE(100000,1,"output1");
  Simulation.reset();
  Simulation.runE(100000,0.01,"output0.01");
  Simulation.reset();
  Simulation.runE(100000,100,"output100");
  */


}
