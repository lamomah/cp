import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation,writers


def plotB(T):
    vx,vy,vz,T,Ekin,Epot = np.genfromtxt("B"+str(T),unpack=True)
    t = np.arange(0,len(vx),1)

    plt.plot(t,np.sqrt((vy**2+vy**2)),label="Schwerpunktsgeschwindigkeit")
    plt.legend(loc="best")
    plt.xlabel("Time in 1/0.0001")
    plt.ylabel("Geschwindigkeit")
    plt.savefig("SV.pdf")
    plt.show()

    plt.plot(t,T,label="T")
    plt.legend(loc="best")
    plt.xlabel("Time in 1/0.0001")
    plt.ylabel("T in K")
    plt.savefig("T.pdf")
    plt.show()

    plt.plot(t,Ekin,label="Ekin")
    plt.plot(t,Epot,label="Epot")
    plt.plot(t,Epot+Ekin,label="Eges")
    plt.legend(loc="best")
    plt.xlabel("Time in 1/0.0001)")
    plt.ylabel("Energie / Einhörner")
    plt.savefig("E.pdf")

    plt.show()

#plotB(0.01)
plotB(1)
#plotB(100)

def lj(r):
    return 4*(r**-12-r**-6)

def ljs(r):
    return 24*(r**6-2)/r**13

r = np.linspace(1,10,1000)

plt.plot(r,lj(r))
plt.plot(r,ljs(r))
plt.ylim(-0.2,0.2)
plt.show()

# c

g01 = np.genfromtxt("C0.010000")[1:]
g1  = np.genfromtxt("C1.000000")[1:]
g100 = np.genfromtxt("C100.000000")[1:]
dr = 0.01
r = np.arange(0,len(g01))*dr

plt.plot(r,g01,label="T=0.01")
plt.legend(loc="best")
plt.xlabel("r")
plt.ylabel("g(r)")
plt.savefig("g1.pdf")
plt.show()

plt.plot(r,g1,label="T=1")
plt.legend(loc="best")
plt.xlabel("r")
plt.ylabel("g(r)")
plt.savefig("g2.pdf")
plt.show()

plt.plot(r,g100,label="T=100")
plt.legend(loc="best")
plt.xlabel("r")
plt.ylabel("g(r)")
plt.savefig("g3.pdf")
plt.show()

# aimation

fig,ax = plt.subplots(1,3)

l1, = ax[0].plot([],[],"ro",animated=True)
l2, = ax[1].plot([],[],"ro",animated=True)
l3, = ax[2].plot([],[],"ro",animated=True)

def init():
    ax[0].set_xlim(0,8)
    ax[0].set_ylim(0,8)
    ax[0].set_xlabel("T = 0.01")
    ax[1].set_xlim(0,8)
    ax[1].set_ylim(0,8)
    ax[1].set_xlabel("T = 1")
    ax[2].set_xlim(0,8)
    ax[2].set_ylim(0,8)
    ax[2].set_xlabel("T = 100")

    return l1,l2,l3,

def update(frame):
    if frame == -1:
        frame = -0.1
    x1,y1,z1, vx,vy,vz, fx,fy,fz = np.genfromtxt("output0.01/"+str(int(frame*10)),unpack=True)
    x2,y2,z2, vx,vy,vz, fx,fy,fz = np.genfromtxt("output1/"+str(int(frame*10)),unpack=True)
    x3,y3,z3, vx,vy,vz, fx,fy,fz = np.genfromtxt("output100/"+str(int(frame*10)),unpack=True)

    l1.set_data(x1[::],y1[::])
    l2.set_data(x2[::],y2[::])
    l3.set_data(x3[::],y3[::])


    return l1,l2,l3,

ani = FuncAnimation(fig,update,frames=range(-1,5000),init_func=init, blit=True,interval = 1)
writer = writers["ffmpeg"]
writer = writer(fps=30, metadata=dict(artist='Me'), bitrate=1800)
ani.save('Animation.mp4', writer=writer)
plt.show()
