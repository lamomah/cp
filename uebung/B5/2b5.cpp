#include <iostream>
#include <cmath>
#include <eigen3/Eigen/Dense>
#include <bitset>
#include <map>
#include <fstream>
#include <chrono>

unsigned long* generateStates(unsigned short N,unsigned long &len, int Sg = 0)
{
  unsigned long n = (unsigned long) std::pow(2,N);
  unsigned long * temp = new unsigned long[n]; // create to large array for states

  len = 0; // counts how many valid states there are
  for(unsigned long i = 0; i<n; i++)
    if(__builtin_popcount(i) == N/2+Sg)  // check if state S_ges = Sg
      temp[len++] = i;

  unsigned long *state = new unsigned long[len]; // resize array
  memcpy(state,temp,len*sizeof(long));
  delete [] temp;

  return state;
}

unsigned long swap(unsigned long S, unsigned short i, unsigned short j)
{
    // extract bits at positions i and j
    unsigned long bit1 =  (S >> i) & 1;
    unsigned long bit2 =  (S >> j) & 1;
 
    // check if bits are equal and move them to position i and j
    unsigned long x = (bit1 ^ bit2);
    x = (x << i) | (x << j);

    // return swaped state
    return  S ^ x;
}


// create map for reverse search of state index
std::map<unsigned long,unsigned long> create_map(unsigned long* States, unsigned long nStates)
{
  std::map<unsigned long,unsigned long> m;
  for(unsigned long i = 0; i<nStates;i++)
    m[States[i]]=i;
  return m;
}

Eigen::MatrixXd generateH(unsigned short N, unsigned long*States, unsigned long nStates)
{
  std::map<unsigned long, unsigned long> lookup = create_map(States,nStates);

  Eigen::MatrixXd H = Eigen::MatrixXd::Zero(nStates,nStates);

  for(unsigned long i = 0; i<nStates; i++)
  {
    H(i,i) -= 4; // -4 
    for(unsigned short j = 0; j < N; j++)
    {
      H(i,lookup[swap(States[i],j,(j+1)%N)]) +=2; // 2P_j,j+1
    } // end for j<N
  }// end for i<nStates
  return H/4;
}

int main(int argc, char *argv[])
{
  std::ofstream file1;
  file1.open("A2c");
  for(unsigned long N = 2; N < 16; N+=2)
  { 
    std::cout << N << std::endl;
    unsigned long nStates(0); // number of generated states
    unsigned long* States = generateStates(N,nStates);


    // measure time for c
    auto start = std::chrono::high_resolution_clock::now();

    // calc eigenvalues
    Eigen::MatrixXd H =generateH(N,States,nStates);
  
    Eigen::EigenSolver<Eigen::MatrixXd> es(H);
    auto elapsed = std::chrono::high_resolution_clock::now() - start;
    
    long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
     file1 << microseconds << "\n";
    

    if(N==10)
    {
      std::ofstream file2;
      file2.open("A2a");
      file2 << H;
      file2.close();

      file2.open("A2b");
      file2 << es.eigenvalues().real();
      file2.close();
    }

    
    delete [] States;
  }
  file1.close();

  return 0;
}
