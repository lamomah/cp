import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

H = np.genfromtxt("A2a")
ev = np.genfromtxt("A2b")
time = np.genfromtxt("A2c")

sort = np.argsort(ev)
ev = ev[sort]

plt.pcolormesh(np.rot90(H))
plt.colorbar()
plt.savefig("A2a.pdf")
plt.show()

ev = ev*np.eye(len(ev))
plt.pcolormesh(np.rot90(ev))
plt.colorbar()
plt.savefig("A2b.pdf")
plt.show()



def t(n,x,a,c):
    return a*n**x+c

N = np.arange(2,len(time)*2+2,2)

parm1, cov1 = curve_fit(t,N,np.log(time))

plt.plot(N,time, label="Daten")
plt.plot(N,np.exp(t(N,*parm1)),label="Fit")
plt.legend(loc="best")
plt.xlabel("N - length of Chain")
plt.ylabel("t / μs")
plt.yscale("log")
plt.savefig("A2c.pdf")
plt.show()

print(parm1, np.sqrt(np.diag(cov1)))
