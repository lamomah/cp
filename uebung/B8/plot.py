import matplotlib.pyplot as plt
import numpy as np
from matplotlib.animation import FuncAnimation

h = 0.01


names = ["+","-"]#,"chaos","quasi","chaos2"]
BaseName = "1b8-rk4_"

for name in names:
    t, vx, vy, rx, ry,T,V = np.genfromtxt(BaseName+name+".txt",unpack = True)

    X1 =  np.sin(rx)
    Y1 = -np.cos(rx)

    X2 = np.sin(rx) + np.sin(ry)
    Y2 = -np.cos(rx) - np.cos(ry)
    plt.plot(X1,Y1 , label = 'Bahn des Pendels 1')
    plt.plot(X2,Y2 , label = 'Bahn des Pendels 2')
    plt.xlabel("rx")
    plt.ylabel("ry")



    plt.legend(loc = 'best')
    plt.grid()
    plt.tight_layout()
    plt.savefig("2b8-rk4_"+name+"trajekt.pdf")
    plt.close()

    fig, ax = plt.subplots()
    #ax.set_ylim(-0.3,0.5)
    plt.plot(t,T,label="T")
    plt.plot(t,V,label="V")
    plt.plot(t,T+V,label="H")
    plt.legend(loc="best")
    plt.savefig("2b8-rk4_"+name+"Energie.pdf")
    plt.close()
    print(max(T))
    print(max(V))
    print(min(T))
    print(min(V))
    # animation


    fig, ax = plt.subplots(1,2)
    ln, = ax[0].plot([], [], 'ro-', animated=True)
    l1, = ax[0].plot([], [], 'g-',lw=0.1, animated=True)
    l2, = ax[0].plot([], [], 'r-', lw = 0.1,animated=True)


    lT, = ax[1].plot([], [], 'r-', label="T", animated=True)
    lV, = ax[1].plot([], [], 'g-', label="V", animated=True)
    lH, = ax[1].plot([], [], 'b-', label="H", animated=True)
     
    lines = [ln,l1,l2,lT,lV,lH]

    ln_xdata = []
    ln_ydata = []
    l1_xdata = []
    l1_ydata = []
    l2_xdata = []
    l2_ydata = []
    lT_ydata = []
    lV_ydata = []
    lH_ydata = []
    t = []


    def init():
        ax[0].set_xlim(-2.1, 2.1)
        ax[0].set_ylim(-2.1, 1) 
        ax[1].set_ylim(-.1, 0.3)
        ax[1].set_xlim(0,len(X1)/10)

        return ln,

    def update(frame):
        frame*=10
        ln_xdata=[0,X1[frame],X2[frame]]
        ln_ydata=[0,Y1[frame],Y2[frame]]
        l1_xdata.append(X1[frame])
        l1_ydata.append(Y1[frame])
        l2_xdata.append(X2[frame])
        l2_ydata.append(Y2[frame])
        lT_ydata.append(T[frame])
        lV_ydata.append(V[frame])
        lH_ydata.append(T[frame]+V[frame])
        t.append(frame)

        ln.set_data(ln_xdata,ln_ydata)
        l1.set_data(l1_xdata,l1_ydata)
        l2.set_data(l2_xdata,l2_ydata)
        lT.set_data(t,lT_ydata)
        lV.set_data(t,lV_ydata)
        lH.set_data(t,lH_ydata)  
        ax[1].set_ylim(-.1+min(min(lV_ydata),min(lT_ydata)), max(lH_ydata)+0.1)

        if frame > len(X1)/10:
            ax[1].set_xlim(frame-len(X1)/10,frame)

        return ln,l1,l2,lT,lV,lH,

    ani = FuncAnimation(fig, update, frames=range(0,int(len(X1)/10)),init_func=init, blit=True,interval=200)
    #=plt.savefig("2b8_rk4_"+name+".gif")
    ani.save("2b8_rk4_"+name+".mp4", writer='imagemagick', fps=30)

    plt.close()
    
