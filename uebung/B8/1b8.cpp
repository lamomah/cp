#include <iostream>
#include <fstream>
#include <cmath>
#include <eigen3/Eigen/Dense>
#include <string>

using namespace std;
using namespace Eigen;

double mu = 0.5;
double lambda = 1.;
double g = 9.81;

Vector2d funktion(Vector2d theta, Vector2d theta_punkt){
  double funktion1 = 1/(1-mu*pow(cos(theta(1)- theta(0)), 2)) * (mu*g*sin(theta(1))*cos(theta(1)-theta(0)) + mu*pow(theta_punkt(0), 2)*sin(theta(1)-theta(0))*cos(theta(1)-theta(0)) - g*sin(theta(0)) + (mu/lambda)*pow(theta_punkt(1), 2)*sin(theta(1)- theta(0)));
  double funktion2 = 1/(1-mu*pow(cos(theta(1)- theta(0)), 2)) * (g*sin(theta(0))*cos(theta(1)-theta(0)) + mu*pow(theta_punkt(1), 2)*sin(theta(1)-theta(0))*cos(theta(1)-theta(0)) - g*sin(theta(1)) + lambda*pow(theta_punkt(0), 2)*sin(theta(1)- theta(0)));
  Vector2d Wert;
  Wert << funktion1, funktion2;
  return Wert;
}
/*/
Vector2d funktion(Vector2d theta, Vector2d theta_punkt){
  double funktion1 = 1./(1.-mu) * ( mu*g*theta(1)-g*theta(0));
  double funktion2 = 1./(1.-mu) * ( g*theta(0)-g*theta(1));
  Vector2d Wert;
  Wert << funktion1, funktion2;
  return Wert;
}*/



void E(Vector2d& theta, Vector2d& thetaP ,double &T, double &V)
{
  Vector2d r1,r2;
  r1 << std::sin(theta(0)),-std::cos(theta(0));
  r2 << std::sin(theta(1)),-std::cos(theta(1));
  r2 +=r1;
  V = 3*g+(r1(1)+r2(1))*g;
  T = 0.5*pow(thetaP(0),2)+0.5*(thetaP.dot(thetaP)+2*thetaP(0)*thetaP(1)*std::cos(theta(0)-theta(1)));

}
void rungekutta4(Vector2d& v, Vector2d& r, double h, double N,std::string Name){
  std::ostringstream strs; //später den double in ein string konvertieren
  strs << Name;
  double t;
  Vector2d k1v, k2v, k3v, k4v, k1r, k2r, k3r, k4r, vp1, rp1;
  std::ofstream file;
  file.open("1b8-rk4_"+strs.str()+".txt");
  Vector2d puffer;
  for(double n = 0.; n <= N; n++){
    t = n*h;

    k1v = funktion(r, v);
    k1r = v;
    puffer = r + 0.5*h*k1r;
    k2v = funktion(puffer, v);
    k2r = v + 0.5*h*k1v;
    puffer = r + 0.5*h*k2r;
    k3v = funktion(puffer, v);
    k3r = v + 0.5*h*k2v;
    puffer = r + h*k3r;
    k4v = funktion(puffer, v);
    k4r = v + h*k3v;

    vp1 = v + (h/6)*(k1v + 2*k2v + 2*k3v + k4v);
    rp1 = r + (h/6)*(k1r + 2*k2r + 2*k3r + k4r);
    double T,V;
    E(r,v,T,V);
    file << t << "\t" << v(0) << "\t" << v(1) << "\t" << r(0) << "\t" << r(1) << "\t" << T << "\t" << V << "\n";

    v = vp1;
    r = rp1;
  }
  file.close();
} // Ende Runge-Kutta 4. Ordnung


int main() { 

  double h(0.01);
  double N(5000);  

  Vector2d theta, theta_punkt; // Startparameter bestimmen
  double theta1, theta2, theta_punkt1, theta_punkt2;

  theta1 = 0.1;
  theta2 = sqrt(2.)*theta1;
  theta_punkt1 = 0.;
  theta_punkt2 = 0.;

  theta << theta1, theta2;
  theta_punkt << theta_punkt1, theta_punkt2;

  rungekutta4(theta_punkt, theta, h, N,"+");

  theta2 *=-1;
  theta(1)= theta2;

  rungekutta4(theta_punkt, theta, h, N,"-");

  theta1 = 3.14;
  theta2 =sqrt(2)*theta1;
  theta(0) = theta1;
  theta(1) = theta2;
  rungekutta4(theta_punkt, theta, h, N,"chaos");

  theta1 = 0;
  theta2 = 0;
  theta_punkt1 = 0.;
  theta_punkt2 = 4.472;
  theta << theta1, theta2;
  theta_punkt << theta_punkt1, theta_punkt2;

  rungekutta4(theta_punkt, theta, h, N,"quasi");

  theta1 = 0;
  theta2 = 0;
  theta_punkt1 = 0.;
  theta_punkt2 = 11.832;
  theta << theta1, theta2;
  theta_punkt << theta_punkt1, theta_punkt2;

  rungekutta4(theta_punkt, theta, h, N,"chaos2");



  
  return 0;
} // Ende main

